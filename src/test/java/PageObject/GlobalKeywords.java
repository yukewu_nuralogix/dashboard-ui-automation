package PageObject;

import DashboardData.DisplayValues;
import DashboardData.ElementLocators;
import DashboardData.InputValues;
import jdk.nashorn.internal.objects.Global;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.testng.Reporter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GlobalKeywords {
    private InputValues input;
    private ElementLocators locator;
    private DisplayValues display;

    GlobalKeywords() {
        input = InputValues.getInstance();
        locator = ElementLocators.getInstance();
        display = DisplayValues.getInstance();
    }

    public boolean PageShouldContainElement(WebDriver driver, By by) throws Exception {
            driver.findElement(by);
            return true;
    }

    public void SafeInputValue(WebDriver driver, By by, String input) throws Exception {
        WebElement element = driver.findElement(by);
        // clear the text field
        element.sendKeys(Keys.CONTROL, Keys.chord("a"));
        element.sendKeys(Keys.BACK_SPACE);
        // input new values
        element.sendKeys(input);
    }

    public void TakeScreenshot(WebDriver driver, String testcase) throws Exception {
        TakesScreenshot scrShot = ((TakesScreenshot)driver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        File DestFile = new File("./screenshots/" + testcase + " (" + timestamp + ").png");
        FileUtils.copyFile(SrcFile, DestFile);
    }

    public String Get_Phone_Number_From_SMS_Website(WebDriver driver) {
        driver.get(input.SMS_Website);
        driver.findElement(By.cssSelector("[href='/country/canada']")).click();
        String phone_number_link = driver.findElement(By.cssSelector("[class='column'] a")).getAttribute("href");
        System.out.println(phone_number_link);
        return phone_number_link.replaceAll("\\D+", "");
    }

    public String Get_Verification_Code_From_SMS(WebDriver driver, String phone_number) throws InterruptedException {
//        Thread.sleep(120000);
        driver.get(input.SMS_Website);
        driver.findElement(By.cssSelector("[href='/country/canada']")).click();
        driver.findElement(By.cssSelector("a[href='/info/" + phone_number + "/']")).click();
        String current_message = "";
        boolean anura_message = false;
        int message_count = 1;
        while (!anura_message) {
            current_message = driver.findElement(By.cssSelector("tbody tr:nth-child("+ Integer.toString(message_count) +") td:nth-child(3)")).getText();
            anura_message = current_message.contains("Anura");
            message_count += 1;
        }
        System.out.println(current_message.replaceAll("\\D+", ""));
        return current_message.replaceAll("\\D+", "");
    }

    public static GlobalKeywords getInstance() {
        return GlobalKeywords.BillPughSingleton.global;
    }

    private static class BillPughSingleton {
        private static final GlobalKeywords global = new GlobalKeywords();
    }
}
