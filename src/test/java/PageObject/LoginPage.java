package PageObject;

import DashboardData.DisplayValues;
import DashboardData.ElementLocators;
import DashboardData.InputValues;
import PageObject.GlobalKeywords;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.testng.Assert.*;

public class LoginPage {
    private ElementLocators locator;
    private InputValues input;
    private GlobalKeywords global;
    private DisplayValues display;

    public LoginPage() {
        locator = ElementLocators.getInstance();
        input = InputValues.getInstance();
        global = GlobalKeywords.getInstance();
        display = DisplayValues.getInstance();
    }

    public void Email_Login_Credentials_Are_Labelled_Correctly(WebDriver driver) throws Exception {
        assertEquals(display.Org_Key_Label, driver.findElement(locator.Org_Key_Label).getText());
        assertEquals(display.Email_Label, driver.findElement(locator.Email_Label).getText());
        assertEquals(display.Password_Label, driver.findElement(locator.Password_Label).getText());
    }

    public void Phone_Login_Credentials_Are_Labelled_Correctly(WebDriver driver) throws Exception {
        assertEquals(display.Org_Key_Label, driver.findElement(locator.Org_Key_Label).getText());
        assertEquals(display.Phone_Label, driver.findElement(locator.Phone_Label).getText());
        assertEquals(display.Verification_Code_Label, driver.findElement(locator.Verification_Code_Label).getText());
    }

    public void Login(WebDriver driver, String orgkey, String username, String password) throws Exception {
        Enter_Org_Key(driver, orgkey);
        global.SafeInputValue(driver, locator.Email, username);
        global.SafeInputValue(driver, locator.Password, password);
        Thread.sleep(2000);
        Click_Login_Button(driver);
    }

    public void Login_As_Super_Admin(WebDriver driver) throws Exception {
        Login(driver, input.SA_Org_Key, input.SA_Username, input.SA_Password);
    }

    public void Login_As_Org_Admin(WebDriver driver) throws Exception {
        Login(driver, input.OA_Org_Key, input.OA_Username, input.OA_Password);
    }

    public void Login_As_Root_Admin(WebDriver driver) throws Exception {
        Login(driver, input.RA_Org_Key, input.RA_Username, input.RA_Password);
    }

    public void Login_As_DFX_Lead(WebDriver driver) throws Exception {
        Login(driver, input.Lead_Org_Key, input.Lead_Username, input.Lead_Password);
    }

    public void Login_As_DFX_Researcher(WebDriver driver) throws Exception {
        Login(driver, input.Res_Org_Key, input.Res_Username, input.Res_Password);
    }

    public void Login_As_DFX_Operator(WebDriver driver) throws Exception {
        Login(driver, input.Ope_Org_Key, input.Ope_Username, input.Ope_Password);
    }

    public void Login_As_Anura_Application_User(WebDriver driver) throws Exception {
        Login(driver, input.Anura_Org_Key, input.Anura_Username, input.Anura_Password);
    }

    public void Login_As_DFX_Trial(WebDriver driver) throws Exception {
        Login(driver, input.Trial_Org_Key, input.Trial_Username, input.Trial_Password);
    }

    public void Password_Can_Be_Visible(WebDriver driver) throws Exception {
        global.SafeInputValue(driver, locator.Password, input.OA_Password);
        String before_status = driver.findElement(locator.Password).getAttribute("type");
        assertEquals(before_status, display.Invisible_Password_Type);
        driver.findElement(locator.Password_Display_Button).click();
        String after_status = driver.findElement(locator.Password).getAttribute("type");
        assertEquals(after_status, display.Visible_Password_Type);
        driver.findElement(locator.Password_Display_Button).click();
        String last_status = driver.findElement(locator.Password).getAttribute("type");
        assertEquals(last_status, display.Invisible_Password_Type);
    }

    public void Log_Into_Another_Organization(WebDriver driver) throws Exception {
        Login(driver, input.SA_Org_Key, input.OA_Username, input.OA_Password);
    }

    public void Login_With_Invalid_Org_Key(WebDriver driver) throws Exception {
        Login(driver, input.Invalid_Org_Key, input.OA_Username, input.OA_Password);
    }

    public void Login_With_Invalid_Password(WebDriver driver) throws Exception {
        Login(driver, input.OA_Org_Key, input.OA_Username, input.Invalid_Password);
    }

    public void Enter_Org_Key(WebDriver driver, String org_key) throws Exception {
        global.SafeInputValue(driver, locator.Org_Key, org_key);
    }

    public void Enter_Phone_Number(WebDriver driver, String phone) throws Exception {
        global.SafeInputValue(driver, locator.Phone, phone);
    }

    public void Click_Login_Button(WebDriver driver) throws Exception {
        driver.findElement(locator.Login_Button).click();
    }

    public void Switch_To_Phone_Number(WebDriver driver) throws Exception {
        String before_status = driver.findElement(locator.Login_Method_Switch).getAttribute("value");
        driver.findElement(locator.Login_Method_Switch).click();
        String after_status = driver.findElement(locator.Login_Method_Switch).getAttribute("value");
        assertNotEquals(after_status, before_status);
    }

    public void Login_Into_Another_Organization_With_Phone(WebDriver driver) throws Exception {
        Enter_Org_Key(driver, input.Phone_Org_Key);
        Enter_Phone_Number(driver, input.Non_Existent_Phone_Number);
        Request_Verification_Code(driver);
    }

    public void User_Not_Found_Error_Is_Displayed(WebDriver driver) throws Exception {
        String actual_error_message = driver.findElement(locator.Non_Existent_User).getText();
        assertEquals(actual_error_message, display.User_Not_Found_Error);
    }

    public void Request_Verification_Code(WebDriver driver) throws Exception {
        driver.findElement(locator.Request_Verification_Code).click();
    }

    public void Enter_Verification_Code(WebDriver driver, String code) throws Exception {
        global.SafeInputValue(driver, locator.Verification_Code, code);
    }

    public void Login_With_Invalid_Phone_Number_Format(WebDriver driver) throws Exception {
        Enter_Org_Key(driver, input.Phone_Org_Key);
        Enter_Phone_Number(driver, input.Invalid_Phone_Number);
        Request_Verification_Code(driver);
    }

    public void Invalid_Number_Error_Is_Displayed(WebDriver driver) throws Exception {
        String label_error = driver.findElement(locator.Phone_Label).getAttribute("class");
        assertTrue(label_error.contains(display.Error_Indicator_Class));
        String error_msg = driver.findElement(locator.Phone_Error).getText();
        assertEquals(error_msg, display.Invalid_Phone_Error);
    }

    public void Login_With_Invalid_Verification_Code(WebDriver driver) throws Exception {
        Enter_Org_Key(driver, input.Phone_Org_Key);
        Enter_Phone_Number(driver, input.Phone_User_Number);
        global.SafeInputValue(driver, locator.Verification_Code, input.Invalid_Verification_Code);
        Thread.sleep(2000);
        Click_Login_Button(driver);
    }

    public void Org_Key_Is_Mandatory(WebDriver driver) throws Exception {
        // Organization Key label should be red. An error message should be displayed.
        String error_label = driver.findElement(locator.Org_Key_Label).getAttribute("class");
        assertTrue(error_label.contains(display.Error_Indicator_Class));
        String error_msg = driver.findElement(locator.Org_Key_Error).getText();
        assertEquals(display.Empty_Org_Key_Error, error_msg);
    }

    public void Email_Is_Mandatory(WebDriver driver) throws Exception {
        // Email Address label should be red. An error message should be displayed.
        String error_label = driver.findElement(locator.Email_Label).getAttribute("class");
        assertTrue(error_label.contains(display.Error_Indicator_Class));
        String error_msg = driver.findElement(locator.Email_Error).getText();
        assertEquals(display.Empty_Email_Error, error_msg);
    }

    public void Password_Is_Mandatory(WebDriver driver) throws Exception {
        // Password label should be red. An error message should be displayed.
        String error_label = driver.findElement(locator.Password_Label).getAttribute("class");
        assertTrue(error_label.contains(display.Error_Indicator_Class));
        String error_msg = driver.findElement(locator.Password_Error).getText();
        assertEquals(display.Empty_Password_Error, error_msg);
    }

    public void Credentials_Are_Invalid(WebDriver driver) throws Exception {
        assertTrue(global.PageShouldContainElement(driver, locator.Invalid_Credential));
        String credential_error_msg = driver.findElement(locator.Invalid_Credential).getText();
        assertEquals(display.Invalid_Credential_Error, credential_error_msg);
    }

    public void Welcome_Message_Displayed(WebDriver driver) throws Exception {
        assertTrue(global.PageShouldContainElement(driver, locator.Page_Header));
        assertEquals(display.Welcome_Header_Text, driver.findElement(locator.Page_Header).getText());
        assertTrue(global.PageShouldContainElement(driver, locator.Welcome_Body));
        assertTrue(driver.findElement(locator.Welcome_Body).getText().contains(display.Welcome_Body_Text_1)
                    && driver.findElement(locator.Welcome_Body).getText().contains(display.Welcome_Body_Text_2));
    }

    public void Correct_Version_Server_Is_Displayed(WebDriver driver) throws Exception {
        if (input.Login_URL.contains("dashboard.deepaffex.ai")) {
            assertEquals(input.Dashboard_Version + " (INTERNATIONAL)", driver.findElement(locator.Version).getText());
        } else if (input.Login_URL.contains("dashboard.deepaffex.cn")) {
            assertEquals(input.Dashboard_Version + "(CHINA)", driver.findElement(locator.Version).getText());
        } else {
            fail();
        }
    }

    public void Correct_Dashboard_User_Info_Is_Displayed(WebDriver driver, String expected_name, String expected_role, String expected_org) throws Exception {
        String actual_name_label = driver.findElement(locator.Name_Label_Side_Menu).getText();
        assertEquals(display.Name_Side_Menu_Label, actual_name_label);
        String actual_username = driver.findElement(locator.Name_Field_Side_Menu).getText();
        assertEquals(expected_name, actual_username);
        String actual_role_label = driver.findElement(locator.Role_Label_Side_Menu).getText();
        assertEquals(display.Role_Side_Menu_Label, actual_role_label);
        String actual_role = driver.findElement(locator.Role_Field_Side_Menu).getText();
        assertEquals(expected_role, actual_role);
        String actual_org_label = driver.findElement(locator.Organization_Label_Side_Menu).getText();
        assertEquals(display.Org_Side_Menu_Label, actual_org_label);
        String actual_org = driver.findElement(locator.Organization_Field_Side_Menu).getText();
        assertEquals(expected_org, actual_org);
    }

    public void Correct_Super_Admin_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.SA_Side_Menu_Name, display.SA_Side_Menu_Role, display.SA_Side_Menu_Org);
    }

    public void Correct_Root_Admin_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.RA_Side_Menu_Name, display.RA_Side_Menu_Role, display.RA_Side_Menu_Org);
    }

    public void Correct_Org_Admin_User_info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.OA_Side_Menu_Name, display.OA_Side_Menu_Role, display.OA_Side_Menu_Org);
    }

    public void Correct_DFX_Lead_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.Lead_Side_Menu_Name, display.Lead_Side_Menu_Role, display.Lead_Side_Menu_Org);
    }

    public void Correct_DFX_Researcher_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.Res_Side_Menu_Name, display.Res_Side_Menu_Role, display.Res_Side_Menu_Org);
    }

    public void Correct_DFX_Operator_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.Ope_Side_Menu_Name, display.Ope_Side_Menu_Role, display.Ope_Side_Menu_Org);
    }

    public void Correct_Anura_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.Anura_Side_Menu_Name, display.Anura_Side_Menu_Role, display.Anura_Side_Menu_Org);
    }

    public void Correct_DFX_Trial_User_Info_Is_Displayed(WebDriver driver) throws Exception {
        Correct_Dashboard_User_Info_Is_Displayed(driver, display.Trial_Side_Menu_Name, display.Trial_Side_Menu_Role, display.Trial_Side_Menu_Org);
    }

    public void Logout(WebDriver driver) throws Exception {
        driver.findElement(locator.Logout_Button).click();
        driver.findElement(locator.Login_Button);
    }

    public void User_Is_Logged_In(WebDriver driver) throws Exception {
        WebElement header = driver.findElement(locator.Page_Header);
        String headerText = header.getText();
        assertEquals(display.Welcome_Header_Text, headerText);
    }

    public boolean is_Login_Page(WebDriver driver) throws Exception {
        boolean flag = false;
        try {
            if (driver.findElement(locator.Login_Button).getText().equals(display.Login_Button_Text)) {
                flag = true;
                return flag;
            }
        } catch (Exception e) {
            return flag;
        }
        return flag;
    }

    public static LoginPage getInstance() {
        return LoginPage.BillPughSingleton.LP;
    }

    private static class BillPughSingleton {
        private static final LoginPage LP = new LoginPage();
    }
}
