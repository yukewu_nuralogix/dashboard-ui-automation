package PageObject;

import DashboardData.ElementLocators;
import DashboardData.InputValues;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class TestSetup {
    private ElementLocators locator;
    private InputValues input;

    public TestSetup() {
        locator = ElementLocators.getInstance();
        input = InputValues.getInstance();
    }

    Browser Browser = new Browser();
    public static WebDriver driver;

    @BeforeMethod
    public void start() throws Exception {
        try {
            driver = Browser.startChrome(input.Login_URL);
            // driver = Browser.startFirefox(URL);
            Thread.sleep(2000);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @AfterMethod
    public void end() throws Exception {
        Browser.tearDownBrowser();
    }
}