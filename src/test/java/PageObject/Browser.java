package PageObject;

import DashboardData.InputValues;
import DashboardData.ElementLocators;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Browser {
    private ElementLocators locator;
    private InputValues input;

    public Browser() {
        locator = ElementLocators.getInstance();
        input = InputValues.getInstance();
    }

    public static WebDriver driver;

    public WebDriver startChrome(String url) throws Exception {
        try {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            ChromeOptions options = new ChromeOptions();
            options.addArguments(input.Run_Mode);
            driver = new ChromeDriver(options);
            driver.get(url);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Error opening Google Chrome" + e);
        }
        return driver;
    }

    public WebDriver startFirefox(String url) throws Exception {
        try {
            System.setProperty("webdriver.geckodriver", "src/main/resources/geckodriver");
            driver = new FirefoxDriver();
            driver.get(url);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Error opening Firefox" + e);
        }
        return driver;
    }

    public void tearDownBrowser() throws Exception {
        try {
            Thread.sleep(2000);
            driver.close();
        } catch (Exception e) {
            System.out.println("Error closing Google Chrome" + e);
        }
    }
}
