package DashboardUI;

import DashboardData.DisplayValues;
import PageObject.TestSetup;
import PageObject.GlobalKeywords;
import PageObject.LoginPage;
import TestRail.ResultUpdate;
import TestRail.TestRails;
import DashboardData.InputValues;
import DashboardData.ElementLocators;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.BeforeMethod;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;

import java.lang.reflect.Method;

public class Login extends ResultUpdate {
//public class Login extends TestSetup {
    Logger logger = LogManager.getLogger("Dashboard UI");

    private ElementLocators locator;
    private InputValues input;
    private GlobalKeywords GK;
    private DisplayValues display;
    private LoginPage LP;

    Login() {
        locator = ElementLocators.getInstance();
        input = InputValues.getInstance();
        GK = GlobalKeywords.getInstance();
        display = DisplayValues.getInstance();
        LP = LoginPage.getInstance();
    }

    @BeforeMethod
    public void beforeTest(ITestContext ctx, Method method) throws NoSuchMethodException {
        Method m = Login.class.getMethod(method.getName());
        if (m.isAnnotationPresent(TestRails.class)) {
            TestRails ta = m.getAnnotation(TestRails.class);
            System.out.println(ta.id());
            ctx.setAttribute("caseId", ta.id());
        }
    }

    @TestRails(id = "899")
    @Test
    public void C899() throws Exception {
        try {
            logger.debug("C899: Users Should Be Able to Login With Email");
            LP.Email_Login_Credentials_Are_Labelled_Correctly(driver);
            logger.debug("Text fields of login credentials are labelled correctly");
            LP.Password_Can_Be_Visible(driver);
            logger.debug("User should be able to view entered password by clicking the visibility switch button");
            LP.Login_As_Org_Admin(driver);
//            LP.Login(driver, "", "", "");
            logger.debug("Login as Org Admin");
            LP.User_Is_Logged_In(driver);
            logger.debug("Dashboard welcome page is displayed");
            LP.Logout(driver);
            logger.debug("User is logged out from Dashboard" );
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C899");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "900")
    @Test
    public void C900() throws Exception {
        try {
            logger.debug("C900: Users Should Be Able to Login With Phone Number");
//            String phone_number = GK.Get_Phone_Number_From_SMS_Website(driver);
//            driver.get(input.Login_URL);
            LP.Switch_To_Phone_Number(driver);
            LP.Enter_Org_Key(driver, input.Phone_Org_Key);
            LP.Enter_Phone_Number(driver, input.Valid_SMS_number);
            LP.Request_Verification_Code(driver);
            driver.findElement(locator.Verification_Code_Sent_Message);
            String verification_code = GK.Get_Verification_Code_From_SMS(driver, input.Valid_SMS_number);
            driver.get(input.Login_URL);
            LP.Switch_To_Phone_Number(driver);
            LP.Enter_Org_Key(driver, input.Phone_Org_Key);
            LP.Enter_Phone_Number(driver, input.Valid_SMS_number);
            LP.Enter_Verification_Code(driver, verification_code);
            LP.Click_Login_Button(driver);
            LP.User_Is_Logged_In(driver);
        } catch (Error | Exception e) {
//            GK.TakeScreenshot(driver, "C900");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "902")
    @Test
    public void C902() throws Exception {
        try {
            logger.debug("C902: Users Should Not Be Able to Login With Manadatory Fields Blank");
            LP.Login(driver, "", "", "");
//            LP.Login_As_Org_Admin(driver);
            logger.debug("Login with all mandatory fields blank");
            LP.Org_Key_Is_Mandatory(driver);
            logger.debug("Mandatory org key error is displayed");
            LP.Email_Is_Mandatory(driver);
            logger.debug("Mandatory email address error is displayed");
            LP.Password_Is_Mandatory(driver);
            logger.debug("Mandatory password error is displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C902");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "903")
    @Test
    public void C903() throws Exception {
        try {
            logger.debug("C903: Users Should Not Be Able to Login If They Do Not Have an Account In That Org (Email)");
            LP.Log_Into_Another_Organization(driver);
//            LP.Login_As_Org_Admin(driver);
            logger.debug("Enter user name and password with wrong organization");
            LP.Credentials_Are_Invalid(driver);
            logger.debug("Invalid credentials error message should be displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C903");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "905")
    @Test
    public void C905() throws Exception {
        try {
            logger.debug("C905: Users Should Not Be Able to Login If They Do Not Have An Account In That Org(Phone)");
            LP.Switch_To_Phone_Number(driver);
            logger.debug("User switch to phone login method");
            LP.Login_Into_Another_Organization_With_Phone(driver);
            logger.debug("User requests code for a number that does not exist in");
            LP.User_Not_Found_Error_Is_Displayed(driver);
            logger.debug("User not found error message is displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C905");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "904")
    @Test
    public void C904() throws Exception {
        try {
            logger.debug("C904:  Users Should Not Be Able to Login With Invalid Password or Org Key");
            LP.Login_With_Invalid_Org_Key(driver);
//            LP.Login_As_Org_Admin(driver);
            logger.debug("Enter user name and password with invalid organization");
            LP.Credentials_Are_Invalid(driver);
            logger.debug("Invalid credentials error message should be displayed");
            LP.Login_With_Invalid_Password(driver);
            logger.debug("Enter user name and org key with invalid password");
            LP.Credentials_Are_Invalid(driver);
            logger.debug("Invalid credentials error message should be displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C904");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "906")
    @Test
    public void C906() throws Exception {
        try {
            logger.debug("C906: Users Should Not Be Able to Login With Invalid Phone or Verification Code(Phone)");
            LP.Switch_To_Phone_Number(driver);
            logger.debug("User switch to phone login method");
            LP.Request_Verification_Code(driver);
            logger.debug("Request verification code without filling in any credentials");
            LP.Org_Key_Is_Mandatory(driver);
            LP.Invalid_Number_Error_Is_Displayed(driver);
            logger.debug("Correct error messages are displayed");
            LP.Login_With_Invalid_Phone_Number_Format(driver);
            logger.debug("Request verification code for a phone number in invalid format");
            LP.Invalid_Number_Error_Is_Displayed(driver);
            logger.debug("Invalid phone number error is displayed");
            LP.Login_With_Invalid_Verification_Code(driver);
            logger.debug("Login with correct org key and phone number, but invalid verification code");
            LP.Credentials_Are_Invalid(driver);
            logger.debug("Invalid credentials error message should be displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C906");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "924")
    @Test
    public void C924() throws Exception {
        try {
            logger.debug("C924: Welcome Page Should Be Displayed for First-Time Login");
            LP.Login_As_Org_Admin(driver);
//            LP.Login(driver, "", "", "");
            logger.debug("Login as Org Admin");
            LP.Welcome_Message_Displayed(driver);
            logger.debug("Welcome messages are displayed");
            LP.Correct_Version_Server_Is_Displayed(driver);
            logger.debug("Correct server and version are displayed");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C924");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "925")
    @Test
    public void C925() throws Exception {
        try {
            logger.debug("C925: Correct Super Admin Info Should Be Displayed In the Side Menu");
            LP.Login_As_Super_Admin(driver);
//            LP.Login_As_Org_Admin(driver);
            logger.debug("Login as Super Admin");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_Super_Admin_User_Info_Is_Displayed(driver);
            logger.debug("Correct Super Admin info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C925");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "929")
    @Test
    public void C929() throws Exception {
        try {
            logger.debug("C929: Correct Org Admin Info Should Be Displayed In the Side Menu");
            LP.Login_As_Org_Admin(driver);
            logger.debug("Login as Org Admin");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_Org_Admin_User_info_Is_Displayed(driver);
            logger.debug("Correct Org Admin info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C929");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "930")
    @Test
    public void C930() throws Exception {
        try {
            logger.debug("C930: Correct Root Admin Info Should Be Displayed In the Side Menu");
            LP.Login_As_Root_Admin(driver);
            logger.debug("Login as Root Admin");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_Root_Admin_User_Info_Is_Displayed(driver);
            logger.debug("Correct Org Admin info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver,"C930");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "931")
    @Test
    public void C931() throws Exception {
        try {
            logger.debug("C931: Correct DFX Lead Info Should Be Displayed In the Side Menu");
            LP.Login_As_DFX_Lead(driver);
            logger.debug("Login as DFX Lead");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_DFX_Lead_User_Info_Is_Displayed(driver);
            logger.debug("Correct DFX Lead info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e ) {
            GK.TakeScreenshot(driver, "C931");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "932")
    @Test
    public void C932() throws Exception {
        try {
            logger.debug("C932: Correct DFX Researcher Info Should Be Displayed In the Side Menu");
            LP.Login_As_DFX_Researcher(driver);
            logger.debug("Login as DFX Researcher");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_DFX_Researcher_User_Info_Is_Displayed(driver);
            logger.debug("Correct DFX Researcher info is dIsplayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C932");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "933")
    @Test
    public void C933() throws Exception {
        try {
            logger.debug("C933: Correct DFX Operator Info Should Be Displayed In the Side Menu");
            LP.Login_As_DFX_Operator(driver);
            logger.debug("Login as DFX Operator");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_DFX_Operator_User_Info_Is_Displayed(driver);
            logger.debug("Correct DFX Operator info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C933");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "934")
    @Test
    public void C934() throws Exception {
        try {
            logger.debug("C934: Correct Anura Application User Info Should Be Displayed In the Side Menu");
            LP.Login_As_Anura_Application_User(driver);
            logger.debug("Login as Anura Application User");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_Anura_User_Info_Is_Displayed(driver);
            logger.debug("Correct Anura Application User info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C934");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }

    @TestRails(id = "935")
    @Test
    public void C935() throws Exception {
        try {
            logger.debug("C935: Correct DFX Trial User Info Should Be Displayed In the Side Menu");
            LP.Login_As_DFX_Trial(driver);
            logger.debug("Login as DFX Trial User");
            LP.User_Is_Logged_In(driver);
            logger.debug("User is logged into Dashboard");
            LP.Correct_DFX_Trial_User_Info_Is_Displayed(driver);
            logger.debug("Correct DFX Trial User info is displayed in side menu");
            logger.debug("Test Case Passed" + System.lineSeparator());
        } catch (Error | Exception e) {
            GK.TakeScreenshot(driver, "C935");
            logger.error("Test Case Failed with ERROR: " + e + System.lineSeparator());
            throw e;
        }
    }
}