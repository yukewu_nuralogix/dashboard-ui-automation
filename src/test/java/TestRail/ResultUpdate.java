package TestRail;

import PageObject.TestSetup;
import PageObject.Browser;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import testrail.APIClient;
import testrail.APIException;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ResultUpdate extends TestSetup {
    String PROJECT_ID = "1";
    //APIClient client = null;
    APIClient client=new APIClient("https://nuralogix.testrail.io/");
    public static Long runId;

    @BeforeSuite
    public void createTestRun(ITestContext ctx) throws IOException, APIException {
        Calendar calendar = Calendar.getInstance();
        System.out.println("client is created");
        // client = new APIClient("https://nuralogix.testrail.io/");
        client.setUser("YukeWu@nuralogix.ai");
        client.setPassword("nuralogix");
        Map data = new HashMap();
        data.put("include_all",true);
        data.put("name","Dashboard UI Test Run "+calendar.getTime());
        data.put("suite_id",28);
        JSONObject c = null;
        c = (JSONObject)client.sendPost("add_run/"+PROJECT_ID,data);
        runId = (Long)c.get("id");

    }

    @AfterMethod
    public void afterTest(ITestResult result, ITestContext ctx) throws IOException, APIException {
        Map data = new HashMap();
        if(result.isSuccess()) {
            data.put("status_id",1);
        }
        else {
            data.put("status_id", 5);
            data.put("comment", result.getThrowable().toString());
        }

        String caseId = (String)ctx.getAttribute("caseId");
        // Long suiteId = (Long)ctx.getAttribute("suiteId");
        System.out.println("My run ID: "+ runId);
        System.out.println("My case ID: "+ caseId);
        System.out.println("client is"+client);
        client.sendPost("add_result_for_case/"+runId+"/"+caseId,data);

    }
}