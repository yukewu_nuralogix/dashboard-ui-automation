package testrailData;

public class testrailResource {

    public String createTestPlan = "{\n" +
            "\t\"name\": \"System test2\",\n" +
            "\t\"entries\": [\n{" +
            "\t\t\t\"suite_id\": 1,\n" +
            "\t\t\t\"include_all\": true\n" +
            "\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\"suite_id\": 3,\n" +
            "\t\t\t\"include_all\": true\n" +
            "\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\"suite_id\": 4,\n" +
            "\t\t\t\"include_all\": true\n" +
            "\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\"suite_id\": 5,\n" +
            "\t\t\t\"include_all\": true\n" +
            "\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\"suite_id\": 6,\n" +
            "\t\t\t\"include_all\": true\n" +
            "\t\t}\t\n" +
            "\t\t\n" +
            "\t]\n" +
            "}\t";
}
