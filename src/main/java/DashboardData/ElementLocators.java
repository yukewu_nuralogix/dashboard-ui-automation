package DashboardData;

import org.openqa.selenium.By;

public class ElementLocators {
    // Login Page
    public By Login_Method_Switch = By.name("loginByPhone");
    public By Org_Key = By.name("identifier");
    public By Email = By.name("email");
    public By Password = By.name("password");
    public By Login_Button = By.id("login_button"); //[class='react-swipeable-view-container'] div[aria-hidden='false']  [type='submit']
    public By Org_Key_Label = By.id("org_key-label"); //#root > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > div > form > div:nth-child(1) > div > label
    public By Org_Key_Error = By.id("org_key-helper-text");  //#root > div > div:nth-child(2) > div > div:nth-child(2) > div > div:nth-child(2) > div > form > div:nth-child(1) > div > p
    public By Email_Label = By.cssSelector("[aria-hidden='false'] [id='email-label']");
    public By Email_Error = By.cssSelector("[aria-hidden='false'] [id='email-helper-text']");
    public By Password_Label = By.cssSelector("[aria-hidden='false'] [id='password-label']");
    public By Password_Error = By.cssSelector("[aria-hidden='false'] [id='password-helper-text']");
    public By Password_Display_Button = By.name("visibility_switch");
    public By Invalid_Credential = By.id("login-error-snack");
    public By Non_Existent_User = By.id("login-code-error-snack");
    public By Phone_Label = By.id("phone-label");
    public By Phone = By.id("phone");
    public By Phone_Error = By.id("phone-helper-text");
    public By Verification_Code_Label = By.id("verification-label");
    public By Verification_Code = By.id("verification");
    public By Request_Verification_Code = By.id("request_code_button");
    public By Verification_Code_Sent_Message = By.id("login-code-success-snack");

    // Welcome Page
    public By Page_Header = By.className("title");
    public By Welcome_Body = By.id("welcome_page_body");
    public By Version = By.id("dashboard_version");

    // Side Menu
    public By Organizations_Tab = By.cssSelector("[to='/org-private']");
    public By Study_Templates_Tab = By.cssSelector("[to='/templates']");
    public By License_Manager_Tab = By.cssSelector("[to='/licenses']");
    public By Users_Tab = By.cssSelector("[to='/org-users']");
    public By Name_Label_Side_Menu = By.id("name_title");
    public By Name_Field_Side_Menu = By.id("user_name");
    public By Role_Label_Side_Menu = By.id("role_title");
    public By Role_Field_Side_Menu = By.id("user_role");
    public By Organization_Label_Side_Menu = By.id("org_title");
    public By Organization_Field_Side_Menu = By.id("user_org");
    public By Logout_Button = By.className("logout");

    // Users Page
    public By Create_New_User_Button = By.cssSelector("[href='/org-users/create']");
    public By Open_Login_Method_Dropdown = By.cssSelector("[id='undefined-undefined-objectObject-63338'] button");



    public static ElementLocators getInstance() {
        return ElementLocators.BillPughSingleton.locator;
    }

    private static class BillPughSingleton {
        private static final ElementLocators locator = new ElementLocators();
    }
}
