package DashboardData;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InputValues {
    // Please pass "headless" or "start-maximized" as RunMode parameter.
    public String Run_Mode = "start-maximized";

    // International url: https://qa-dashboard.deepaffex.ai/
    // Chinese url: https://qa-dashboard.deepaffex.cn/
    public String Login_URL ="https://dev.dashboard.deepaffex.ai/";
    public String Dashboard_Version = "v1.17.5";

    // Login Credentials
    // Super Admin for Deepaffex
    public String SA_Org_Key = "deepaffex";
    public String SA_Username = "admin@deepaffex.ai";
    public String SA_Password = "L18fDIjRgvA8it9";

    // Root Admin for Nuralogix
    public String RA_Org_Key = "nuralogix";
    public String RA_Username = "admin@deepaffex.ai";
    public String RA_Password = "3UKu42b0btAN";

    // Org Admin for Nuralogix
    public String OA_Org_Key = "nuralogix";
    public String OA_Username = "dfxv3api2019@gmail.com";
    public String OA_Password = "123456";

    // DFX Lead for Nuralogix
    public String Lead_Org_Key = "nuralogix";
    public String Lead_Username = "dfxlead2019@gmail.com";
    public String Lead_Password = "123456";

    // DFX Researcher for Nuralogix
    public String Res_Org_Key = "nuralogix";
    public String Res_Username = "dfxresearchers@gmail.com";
    public String Res_Password = "123456";

    // DFX Operator for Nuralogix
    public String Ope_Org_Key = "nuralogix";
    public String Ope_Username = "dfxoperators@gmail.com";
    public String Ope_Password = "123456";

    // Anura Application User for Nuralogix
    public String Anura_Org_Key = "nuralogix";
    public String Anura_Username = "anurausertest@gmail.com";
    public String Anura_Password = "123456";

    // DFX Trial for Demo
    public String Trial_Org_Key = "demo";
    public String Trial_Username = "yukewu@nuralogix.ai";
    public String Trial_Password = "123456";

    // Phone Number Login
    public String Phone_Org_Key = "nuralogix";
    public String Invalid_Verification_Code = "123456";
    public String Phone_User_Number = "12269785719";
    public String Non_Existent_Phone_Number = "11234567890";
    public String Invalid_Phone_Number = "123456789";
    public String SMS_Website = "https://smsreceivefree.com/";
    public String Valid_SMS_number = "19029050612";

    // Invalid Credentials
    public String Invalid_Org_Key = "invalid";
    public String Invalid_Password = "invalid";


    public static InputValues getInstance() {
        return InputValues.BillPughSingleton.input;
    }

    private static class BillPughSingleton {
        private static final InputValues input = new InputValues();
    }
}
