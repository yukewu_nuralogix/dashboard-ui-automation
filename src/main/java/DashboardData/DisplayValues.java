package DashboardData;

public class DisplayValues {
    // Login Page
    public String Org_Key_Label = "Organization Key";
    public String Email_Label = "Email Address";
    public String Password_Label = "Password";
    public String Invisible_Password_Type = "password";
    public String Visible_Password_Type = "text";
    public String Phone_Label = "Phone number";
    public String Verification_Code_Label = "Verification code";
    public String Login_Button_Text = "Login";
    public String Empty_Org_Key_Error = "Please enter your organization key";
    public String Empty_Email_Error = "Please enter a valid email address";
    public String Invalid_Phone_Error = "Please enter a valid phone number";
    public String Empty_Password_Error = "Please enter your password";
    public String Invalid_Credential_Error = "Invalid Credentials! Please try again.";
    public String User_Not_Found_Error = "User not found! Please try again.";
    public String Error_Indicator_Class = "Mui-error";

    // Welcome Page
    public String Welcome_Header_Text = "DFX Dashboard";
    public String Welcome_Body_Text_1 = "Welcome to DeepAffex user dashboard!";
    public String Welcome_Body_Text_2 = "Note: Please make sure to disable adblocker and allow popups on this site if you are planning to download CSV files.";

    // Side Menu
    public String Dashboard_Version = "v1.17.5";
    public String Name_Side_Menu_Label = "Name";
    public String Role_Side_Menu_Label = "Role";
    public String Org_Side_Menu_Label = "Organization";

    public String SA_Side_Menu_Name = "SUPER ADMIN";
    public String SA_Side_Menu_Role = "DFX Super";
    public String SA_Side_Menu_Org = "DeepAffex";

    public String RA_Side_Menu_Name = "ROOT ADMIN";
    public String RA_Side_Menu_Role = "DFX Admin";
    public String RA_Side_Menu_Org = "NuraLogix Corporation";

    public String OA_Side_Menu_Name = "ORG ADMIN";
    public String OA_Side_Menu_Role = "DFX Org. Admin";
    public String OA_Side_Menu_Org = "NuraLogix Corporation";

    public String Lead_Side_Menu_Name = "DFX Lead";
    public String Lead_Side_Menu_Role = "DFX Lead";
    public String Lead_Side_Menu_Org = "NuraLogix Corporation";

    public String Res_Side_Menu_Name = "DFX Researcher";
    public String Res_Side_Menu_Role = "DFX Researcher";
    public String Res_Side_Menu_Org = "NuraLogix Corporation";

    public String Ope_Side_Menu_Name = "DFX Operator";
    public String Ope_Side_Menu_Role = "DFX Operator";
    public String Ope_Side_Menu_Org = "NuraLogix Corporation";

    public String Anura_Side_Menu_Name = "Anura User";
    public String Anura_Side_Menu_Role = "Anura Application User";
    public String Anura_Side_Menu_Org = "NuraLogix Corporation";

    public String Trial_Side_Menu_Name = "Yuke Wu";
    public String Trial_Side_Menu_Role = "DFX Trial Account";
    public String Trial_Side_Menu_Org = "Demo Organization";

    // Users Page
    public String Users_Page_Title = "User Accounts";

    public static DisplayValues getInstance() {
        return DisplayValues.BillPughSingleton.display;
    }

    private static class BillPughSingleton {
        private static final DisplayValues display = new DisplayValues();
    }
}
